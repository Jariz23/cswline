FROM openjdk:8-jdk-alpine
RUN addgroup -S mqm && adduser -S latitude -G mqm
USER latitude:mqm
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]