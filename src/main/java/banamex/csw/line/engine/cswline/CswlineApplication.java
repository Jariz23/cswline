package banamex.csw.line.engine.cswline;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.jms.annotation.EnableJms;

@SpringBootApplication
@EnableJms
public class CswlineApplication {
	
	public static void main(String[] args) {
		 ConfigurableApplicationContext context = SpringApplication.run(CswlineApplication.class, args);
	}
	
	
	 
}
