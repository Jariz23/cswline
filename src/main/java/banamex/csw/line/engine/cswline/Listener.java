package banamex.csw.line.engine.cswline;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.IntegerSerializer;
import org.apache.kafka.common.serialization.StringSerializer;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.stereotype.Component;
@Component
public class Listener {
	static final String qName = "cswqueue";
	  static boolean warned = false;

	  
	@JmsListener(destination = Listener.qName)
	  public void receiveMessage(String msg) {
		  
	    infinityWarning();

	    System.out.println();
	    System.out.println("========================================");
	    System.out.println("Received message is: " + msg);
	    System.out.println("========================================");
	    
	    System.out.println("Sending message to KAFKA is: " + msg);
	    KafkaTemplate<Integer, String> template =  this.createTemplate();
	    
	        template.send("test", msg);
	        System.out.println(" message sent to KAFKA");

	  }

		private KafkaTemplate<Integer, String> createTemplate() {
		    Map<String, Object> senderProps = senderProps();
		    ProducerFactory<Integer, String> pf =
		              new DefaultKafkaProducerFactory(senderProps);
		    KafkaTemplate<Integer, String> template = new KafkaTemplate<>(pf);
		    return template;
		}   
		
		private Map<String, Object> senderProps() {
		    Map<String, Object> props = new HashMap<>();
		    props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "192.168.1.78:9092");
		    props.put(ProducerConfig.RETRIES_CONFIG, 0);
		    props.put(ProducerConfig.BATCH_SIZE_CONFIG, 16384);
		    props.put(ProducerConfig.LINGER_MS_CONFIG, 1);
		    props.put(ProducerConfig.BUFFER_MEMORY_CONFIG, 33554432);
		    props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, IntegerSerializer.class);
		    props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		    return props;
		}
	  void infinityWarning() {
	    if (!warned) {
	      warned = true;
	      System.out.println();
	      System.out.println("========================================");
	      System.out.println("MQ JMS Listener started for queue: " + Listener.qName);
	      System.out.println("NOTE: This program does not automatically end - it continues to wait");
	      System.out.println("      for more messages, so you may need to hit BREAK to end it.");
	      System.out.println("========================================");
	    }
	  }
	}
